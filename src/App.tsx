import './App.css';
import { ProductsProvider } from './providers/productsProvider';

import Header from './components/header';
import Routes from './routes';

function App() {
  return (
    <ProductsProvider>
      <div className="App">
        <header className="App-header">
          <Header/>
          <Routes/>
        </header>
      </div>
    </ProductsProvider>    
  );
}

export default App;

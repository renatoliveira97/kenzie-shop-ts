import './style.css';

import { Badge } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { useCart } from '../../providers/cartProvider';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';

const Header = () => {

    const { cart } = useCart();

    const history = useHistory();

    const goToCart = () => {
        history.push('/cart');
    }

    const goToHome = () => {
        history.push('/');
    }

    return (
        <header>
            <div className='cabecalho'>
                <h1 onClick={goToHome}>Kenzie Shop</h1>
                <Badge className='carrinho' badgeContent={cart !== undefined ? cart.length : 0} color='error' onClick={goToCart}>
                    <ShoppingCartIcon/>
                </Badge>
            </div>   
            <hr></hr>         
        </header>
    );
}

export default Header;
import './style.css';

import { useHistory } from 'react-router-dom';
import { Product } from '../../providers/productsProvider';
import { useCart } from '../../providers/cartProvider';
import Button from '../../components/button';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';

const Cart = () => {

    const { cart, removeFromCart } = useCart();

    const history = useHistory();

    const goToHome = () => {
        history.push('/');
    }

    const goToEnd = () => {
        history.push('/finish')
    }

    return (
        <div className='cart'>
            <div className="cart-container">
                <div className="cart-header">
                    <h4>Produto</h4>
                </div>
                <hr></hr>
                {cart !== undefined && cart.map((item: Product) => (
                    <li className='cart-li' key={item.id}>
                        <figure>
                            <img className='cart-img' src={item.image_url} alt={item.name}/>
                        </figure>
                        <div className='cart-item-info'>
                            <strong>{item.name}</strong>
                            <span>{item.price}</span>
                        </div>
                        <DeleteForeverIcon className='cart-delete-item' onClick={() => removeFromCart(item)}/>
                    </li>
                ))}
            </div>
            <div className="cart-total">
                <h2>Resumo do pedido</h2>
                <hr></hr>
                {cart !== undefined ? cart.length === 0 ? (
                    <div className='cart-no-items'>
                        <span><strong>Não há itens no carrinho, que tal acessar a loja para comprar algo?</strong></span>
                        <Button onClick={goToHome}>Acessar a loja</Button>
                    </div>
                ) : (
                    <div className='cart-with-items'>
                        <div className='cart-with-items-div'>
                            <h3>{cart.length} items no carrinho</h3>
                            <span>{cart.reduce((x, y) => y.price + x, 0).toFixed(2)}</span>
                        </div>                        
                        <Button onClick={goToEnd}>Finalizar compra</Button>
                    </div>
                ) : (
                    <div className='cart-no-items'>
                        <span><strong>Não há itens no carrinho, que tal acessar a loja para comprar algo?</strong></span>
                        <Button onClick={goToHome}>Acessar a loja</Button>
                    </div>
                )}
            </div>
        </div>
        
    );
}

export default Cart;
import './style.css';

import { Redirect } from 'react-router-dom';

interface Auth {
    authenticated: boolean;
}

const Finish = ({ authenticated }: Auth) => {

    if (!authenticated) {
        return <Redirect to="/login"/>;
    }

    return (
        <>
            <h1>Compra finalizada</h1>
        </>
    );
}

export default Finish;
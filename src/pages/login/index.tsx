import './style.css';

import * as yup from 'yup';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import { Redirect, useHistory } from 'react-router-dom';

import axios from 'axios';

interface Auth {
    authenticated: boolean;
    setAuth: () => void;
}

interface Data {
    email: string;
    password: string;
}

const Login = ({ authenticated, setAuth }: Auth) => {

    const formSchema = yup.object().shape({
        email: yup.string().required("E-mail obrigatório").email("E-mail inválido"),
        password: yup.string().required("Senha obrigatória").min(8, "Senha deve ter ao menos 8 caracteres"),
    });

    const {
        register,
        handleSubmit,
        formState: { errors }
    } = useForm({
        resolver: yupResolver(formSchema)
    });

    const history = useHistory();

    const onSubmit = (data: Data) => {
        axios.post('https://kenzieshop2.herokuapp.com/login', data)
             .then((response) => {
                const { accessToken } = response.data;

                localStorage.setItem('@kenzieshop:token', JSON.stringify(accessToken));
                setAuth();

                return history.push('/finish');
             })
             .catch((err) => console.log(err));
    }

    if (authenticated) {
        return <Redirect to="/"/>;
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <input
                type='text'
                placeholder='Email'
                {...register('email')}/>
            <p className="yup_error">{errors.email?.message}</p>
            <input
                type='password'
                placeholder='Digite sua senha'
                {...register('password')}/>
            <p className="yup_error">{errors.password?.message}</p>
            <button type='submit'>Entrar</button>
            <p>Não tem uma conta? <a href='/cadastro'><strong>Cadastre-se</strong></a></p>
        </form>
    );
}

export default Login;
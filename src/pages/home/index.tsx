import './style.css';

import { useProducts, Product } from '../../providers/productsProvider';
import { useCart } from '../../providers/cartProvider';

import Button from '../../components/button';

const Home = () => {

    const { products } = useProducts();
    const { addToCart } = useCart();

    return (
        <div className='container'>
                <div className='product-list'>
                    {products.map((product: Product) => (
                        <li key={product.id}>
                            <figure>
                                <img src={product.image_url} alt={product.name}/>
                            </figure>
                            <hr></hr>
                            <div className='product-name'>
                                <strong>{product.name}</strong>
                            </div>                            
                            <div>
                                <span>{product.price}</span>
                            </div>
                            <Button onClick={() => addToCart(product)}>
                                <span>Adicionar ao carrinho</span>
                            </Button>
                        </li>
                    ))}
                </div>
        </div>
    );
}

export default Home;
import './style.css';

import * as yup from 'yup';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import { useHistory, Redirect } from 'react-router-dom';

import axios from 'axios';

interface Auth {
    authenticated: boolean;
}

interface Data {
    username: string;
    email: string;
    password: string;
}

const Cadastro = ({ authenticated }: Auth) => {

    const formSchema = yup.object().shape({
        username: yup.string().required("Nome obrigatório"),
        password: yup.string().required("Senha obrigatória").min(8, "Senha deve ter ao menos 8 caracteres"),
        email: yup.string().required("E-mail obrigatório").email("E-mail inválido"),
    });

    const {
        register,
        handleSubmit,
        formState: { errors }
    } = useForm({
        resolver: yupResolver(formSchema)
    });
    
    const history = useHistory();

    const onSubmit = (data: Data) => {
        axios.post('https://kenzieshop2.herokuapp.com/register', data)
             .then((response) => history.push("/login"))
             .catch((err) => console.log(err));
    }

    if (authenticated) {
        return <Redirect to="/"/>;
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <input
                type='text'
                placeholder='Usuário'
                {...register('username')}/>
            <p className="yup_error">{errors.username?.message}</p>
            <input
                type='password'
                placeholder='Digite sua senha'
                {...register('password')}/>
            <p className="yup_error">{errors.password?.message}</p>
            <input
                type='text'
                placeholder='Email'
                {...register('email')}/>
            <p className="yup_error">{errors.email?.message}</p>
            <button type='submit'>Cadastrar-se</button>
            <p>Já tem uma conta? Faça o <a href='/login'><strong>login</strong></a></p>               
        </form>
    );
}

export default Cadastro;
import { useState, useEffect } from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from '../pages/home';
import Cart from '../pages/cart';
import Finish from '../pages/finish';
import Login from '../pages/login';
import Cadastro from '../pages/cadastro';

const Routes = () => {

    const [authenticated, setAuthenticated] = useState(false);

    const setAuth = () => {
        setAuthenticated(true);
    }

    useEffect(() => {
        const token = JSON.parse(localStorage.getItem('@kenzieshop:token') || '{}');

        if (token.length > 0) {
            return setAuthenticated(true);
        }
    }, [authenticated]);

    return (
        <Switch>
            <Route path="/" exact component={Home}/>
            <Route path="/cart" component={Cart}/>
            <Route path="/finish">
                <Finish authenticated={authenticated}/>
            </Route>
            <Route path="/login">
                <Login authenticated={authenticated} setAuth={setAuth}/>
            </Route>
            <Route path="/cadastro">
                <Cadastro authenticated={authenticated}/>
            </Route>
        </Switch>
    );
}

export default Routes;
import { createContext, ReactNode, useContext, useState } from "react";
import { Product } from '../productsProvider';
 
interface CartProvidersProps {
    children: ReactNode;
}

interface CartProviderData {
    cart: Product[];
    addToCart: (product: Product) => void;
    removeFromCart: (product: Product) => void;
}

const CartContext = createContext<CartProviderData>(
    {} as CartProviderData
);

export const CartProvider = ({
    children,
}: CartProvidersProps) => {
    const [cart, setCart] = useState<Product[]>([] as Product[]);

    const addToCart = (product: Product) => {
        setCart([...cart, product]);
    };

    const removeFromCart = (productToBeDeleted: Product) => {
        const newList = cart.filter((product) => product.id !== productToBeDeleted.id);
        setCart(newList);
    }

    return (
        <CartContext.Provider value={{ cart, addToCart, removeFromCart }}>
            {children}
        </CartContext.Provider>
    );
}

export const useCart = () => useContext(CartContext);

import { createContext, ReactNode, useContext, useState, useEffect } from "react";
import axios from 'axios';

export interface Product {
    name: string;
    image_url: string;
    price: number;
    description: string;
    id: number;
}

interface ProductsProvidersProps {
    children: ReactNode;
}

interface ProductsProviderData {
    products: Product[];
}

const ProductsContext = createContext<ProductsProviderData>(
    {} as ProductsProviderData
);

export const ProductsProvider = ({
    children,
}: ProductsProvidersProps) => {
    const [products, setProducts] = useState<Product[]>([] as Product[]);

    const loadProducts = () => {
        axios
            .get<Product[]>('https://kenzieshop2.herokuapp.com/products')
            .then((response) => {
                setProducts(response.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }

    useEffect(() => {
        loadProducts();
    }, []);

    return (
        <ProductsContext.Provider value={{ products }}>
            {children}
        </ProductsContext.Provider>
    );
}

export const useProducts = () => useContext(ProductsContext);

import { ReactNode } from "react";

import { ProductsProvider } from './productsProvider';
import { CartProvider } from './cartProvider';

interface ProvidersProps {
  children: ReactNode;
}

const Providers = ({ children }: ProvidersProps) => {
  return (
    <ProductsProvider>
      <CartProvider>
        {children}
      </CartProvider>      
    </ProductsProvider>);
};

export default Providers;
